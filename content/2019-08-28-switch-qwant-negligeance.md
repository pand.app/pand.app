+++
title = "Est ce que passer de Google à Qwant peut être de la négligence ?"
date = 2019-08-28
category = "IT"

[taxonomies]
tags = ["IT", "Qwant", "Google"]
+++

Aujourd'hui, un ami m'a posé la question suivante, de manière informelle : "Penses-tu que je devrais faire passer mes salariés sur Qwant ?"

## Contexte

Pour donner le contexte de la société, en question sans la nommer, considérez que :

* L'entreprise en question a aujourd'hui très peu d'adhérence à Google, n'utilise pas dans ses processus les applications de Google, et n'a aucun contrat avec Google.
* L'entreprise est en France.
* L'entreprise emploi entre 40 et 50 personnes, sur un secteur tertiaire

## Pourquoi changer ?

Les éléments de réponse sont nombreux ici, le premier étant un effet de mode, les administrations et beaucoup de grosses entreprises passent de Google à Qwant ces derniers mois. Ensuite, je qualifie cette décision d'élan patriotique.

## Le coût du changement

En soit, changer de moteur de recherche, c'est simple, il suffit de ne plus utiliser un moteur, et utiliser l'autre, le temps de formation est quasi nul (pour toutes les requêtes simples, on va dire 99% de l'usage).

Le coût n'est cependant pas nul. On peut considérer que l'utilisation d'un moteur de recherche ne s'arrête pas à la requête mais va jusqu'au moment où l'information est trouvée.

Et là, on a des différences fondamentales entre les différents moteurs de recherche, le premier c'est la puissance de leur index / leur compréhension des pages web / leur compréhension de la requête de l'utilisateur, bref, la puissance du moteur lui même.

Ensuite, on va avoir les outils permettant de ne pas changer de page, qui donnent un accès plus rapide à l'information.

Enfin, point non négligeable, l'habitude de l'utilisateur. C'est un investissement énorme que de changer les habitudes de quelqu'un, ça va plus loin que juste l'adresse du site, c'est surtout vis à vis de l'interface, de balayer l'écran pour trouver l'information que l'on cherche.

## L'avenir ?

Et là, se pose la question du "Mises-t-on sur le bon cheval ?", et cette question est légitime. Aujourd'hui, on connait la position de Qwant, on sait ce qu'elle a été, mais que sera-t-elle ? Il s'agit d'une société avec de l'ambition, et de l'investissement. La direction sera peut être amenée à changer, le partage du capital également.

Le problème, c'est que l'éthique c'est comme la vérité, chacun a ses opinions, ses valeurs, et son éthique. On ne peut garantir que l'éthique du Qwant d'aujourd'hui sera l'éthique du Qwant de demain. Je soulignerai que le Google d'hier avait plus d'éthique que le Google d'aujourd'hui. Qu'en sera-t-il de Qwant ?

La question d'utiliser Qwant pour la souveraineté est également à questionner : qu'en sera-t-il demain ? 

## Et searx ?

Une bonne alternative pour une entreprise serait alors de se tourner vers [searx](https://searx.me/), il s'agit d'un métamoteur de recherche, ainsi on pourrait y intégrer les moteurs que l'on souhaite exploiter, ajouter d'éventuels indexes internes (wiki interne ?), et surtout, quand le vent tourne, on pourrait modifier cela la config sans changer le comportement des salariés.

Le principal inconvénient c'est qu'il faut monter le service dans son réseau pour qu'il soit vraiment intéressant, et pour avoir le contrôle de sa configuration.

Je crois qu'on oublie un peu trop vite cet acteur qui existe depuis longtemps...


