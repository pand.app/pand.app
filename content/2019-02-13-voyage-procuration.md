+++
title = "Voyager par procuration"
date = 2019-02-13
category = "Écologie"

[taxonomies]
tags = ["écologie"]
+++

L'objectif de réduction des émissions de Gaz à Effet de Serre (GES) a plusieurs leviers d'action.
Un de ceux-là c'est la [réduction des trajets en avion](https://fr.wikipedia.org/wiki/Impact_climatique_du_transport_a%C3%A9rien). Cette partie de l'activité humaine liée au transport pèserait au moins à 2% dans les émissions de GES.

La solution est simple : ne plus prendre l'avion.

## Pression sociale

Un des problèmes dont je suis convaincu (oui, convaincu, je suis pas un chercheur, ce blog n’a pas vocation à être pointilleux, venez me faire changer d'avis !), c'est la pression sociale induite par le voyage.

Je m'explique : Comme signe extérieur d'épanouissement, voire de richesse, il faut se montrer et se mettre en valeur en vacances. Le paraître étant plus fort que le réel.

Ainsi, il faut aller là où tout le monde va et là où personne ne va, du Japon, aux États-Unis, en passant par les déserts humains.

## Le voyage forme la jeunesse

Je pense qu'il faut prendre l'adage au sens étymologique. C'est le voyage qui formerait, et non la destination. Je ne sais pas d'où dans le temps provient le dicton, mais il aurait été repris par Alphonse Allais en 1854, donc il date de bien avant cela.

Ça voudrait dire que le proverbe vient d'une époque où voyager prenait du temps, ce qui implique une logistique bien plus importante que notre petit bagage.

En éliminant l'avion, un voyage au Japon pourrait vite devenir un défi, et l'organisation nécessaire, la découverte des cultures, etc... permettrait de s'enrichir bien plus.

## Considérer le voyage par procuration

Peut-être un élément de réponse à ce besoin de voyage, de découverte, pourrait être de voyager par procuration. En envoyant quelqu'un pour qui sa mission serait de voyager, découvrir, et tenir compte de son voyage à ceux l'ayant soutenu. En ce sens, ce que fait Axolot, avec [Étranges Escales](https://www.youtube.com/playlist?list=PL61mLQTC4oQchGx8yZ300IJGGX_jD3r92) peut être un élément de réponse ? Et si l'étape suivante était de lui demander d'aller voir des lieux, quitte à financer en sus les lieux qu'on souhaite qu'il nous fasse découvrir ?

Même si le format vidéo est lui même polluant, demandant beaucoup de ressources informatiques, il l'est très probablement moins que le voyage en lui même.

Dans tous les cas, pour voyager par procuration, il faudrait accepter de ne pas vivre le voyage. Est-ce un sacrifice acceptable ? Peut-on l'appliquer aux voyages en voiture, en train... ? Quels voyages faire ou ne pas faire ?
