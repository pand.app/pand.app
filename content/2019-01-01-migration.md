+++
title = "Migration Wordpress vers Zola"
date = 2019-01-01
category = "Écologie"

[taxonomies]
tags = ["écologie"]
+++

Au revoir [Wordpress](https://fr.wordpress.org/), et bonjour [Zola](https://github.com/getzola/zola) !

Je viens de migrer mon blog qui était hébergé sur un wordpress, avec une stack d'OpenVZ PHP/Apache/MariaDB vers Zola.
J'ai juste gardé deux articles de 2013 pour l'anecdote, les autres étaient liés à des versions spécifiques de logiciels, et ne valaient pas le coup d'être migrés.

Changement de plateforme, dans le but premier de proposer une interface beaucoup plus légère et donc plus eco-conçue, servant des fichiers statiques HTML, générés par Zola depuis des fichiers markdown.

L'objectif annexe étant de diminuer le besoin de maintenance du blog, et la surface d'attaque.

Ces changements suivent un changement de ligne édito du blog, je vais l'utiliser de manière générale pour plein de sujets non-techniques, qui n'auraient pas leur place sur le blog de [Qonfucius](https://qonfucius.blog). Les publications ne seront pas régulières, pourront être en anglais ou en français mais pas les deux. Globalement, tout comme les réseaux sociaux mais pour les billets qui ne rentreraient pas sur ces derniers. Pas de commentaires sur le blog, donc pour réagir ça peut se faire par le biais de ces dits réseaux.

